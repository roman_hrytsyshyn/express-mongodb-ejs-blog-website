const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');

// routes
const mainRoutes = require('./routes/mainRoutes');
const blogRoutes = require('./routes/blogRoutes');

// express app
const app = express();
const PORT = 3000;

// connect to MongoDB
const dbURI = 'mongodb+srv://netninja:pg6ac6nCDrPKh4k4@nodejs-ninja.u5bzn8g.mongodb.net/nodejs-ninja-blogs?retryWrites=true&w=majority';
mongoose.connect(dbURI, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => {
        console.log('connected to MongoDB');
        app.listen(PORT)
    })
    .catch(console.log);


// register view engine
app.set('view engine', 'ejs');
app.set('views', 'pages');

// middleware and static files
app.use(express.static('public'));
app.use(express.urlencoded({ extended: true }));
app.use(morgan('tiny'));

// routes
app.use('/', mainRoutes);
app.use('/about', mainRoutes);
app.use('/blogs', blogRoutes);

// redirect
app.get('/about-us', (req, res) => {
    res.redirect('/about');
});

// error
app.use((req, res) => {
    res.status(404).render('error', { title: 'Error' });
});
