const getRoot = (req, res) => {
    res.redirect('/blogs');
}

const getAbout = (req, res) => {
    res.render('about', { title: 'About' });
}

module.exports = { getRoot, getAbout };
