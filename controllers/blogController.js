const Blog = require('../models/blog');

const getBlogs = (req, res) => {
    Blog.find()
        .sort({ createdAt: -1 })
        .then((result) => {
            res.render('index', { title: 'All Blogs', blogs: result })
        })
        .catch((error) => console.log(error))
}

const postBlogs = (req, res) => {
    const blog = new Blog(req.body);
    blog.save()
        .then(() => res.redirect('/blogs'))
        .catch(error => console.log(error))
}

const getCreate = (req, res) => {
    res.render('create', { title: 'Create Blog' });
}

const getBlogID = (req, res) => {
    const id = req.params.id;
    Blog.findById(id)
        .then((result) => {
            res.render('details', { title: 'Blog Details', blog: result })
        })
        .catch((error) => console.log(error))
}

const deleteBlogID = (req, res) => {
    const id = req.params.id;
    Blog.findByIdAndDelete(id)
        .then(() => res.json({ redirect: '/blogs' }))
        .catch((error) => console.log(error));
}

module.exports = { getBlogs, postBlogs, getCreate, getBlogID, deleteBlogID };
