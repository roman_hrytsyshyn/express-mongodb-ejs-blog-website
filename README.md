Start this project:

1. Clone the project to your folder 
git clone https://gitlab.com/roman_hrytsyshyn/express-mongodb-ejs-blog-website.git
2. Install modules by running next command in a terminal
npm install
3. Start the local server also by running next command in a terminal
npm start
4. Open the next link in your browser
http://localhost:3000
