const express = require('express');
const router = express.Router();
const { getRoot, getAbout } = require('../controllers/mainController');

router.get('/', getRoot);

router.get('/about', getAbout);

module.exports = router;
