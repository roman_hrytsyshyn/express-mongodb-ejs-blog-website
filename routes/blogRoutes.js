const express = require('express');
const router = express.Router();
const { getBlogs, postBlogs, getCreate, getBlogID, deleteBlogID } = require('../controllers/blogController');

router.get('/', getBlogs);

router.post('/', postBlogs);

router.get('/create', getCreate);

router.get('/blog/:id', getBlogID);

router.delete('/blog/:id', deleteBlogID);

module.exports = router;
